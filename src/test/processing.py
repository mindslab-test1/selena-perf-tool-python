import time
import os
import random

from multiprocessing import Process, Queue


#
def do_work(x, q):
    for i in range(0, 10):
        time.sleep(random.random())
        # print("현재 값은 ", i, "입니다.")
        q.put([1, 0])


def do_stat(q):
    total_cnt = 0
    total_fail_cnt = 0
    tps_cnt = 0
    fail_cnt = 0

    before_time = time.time()
    while True:
        if time.time() - before_time > 1:
            print_stat(total_cnt, total_fail_cnt, tps_cnt, fail_cnt)

            before_time = time.time()
            # print('data => {} / {}'.format(tps_cnt, fail_cnt))
            tps_cnt = 0
            fail_cnt = 0

        if q.qsize() == 0:
            continue

        data = q.get()
        if data[0] == -1 and data[1] == -1:
            print_stat(total_cnt, total_fail_cnt, tps_cnt, fail_cnt)
            break

        total_cnt = total_cnt + 1
        total_fail_cnt = total_fail_cnt + data[1]
        tps_cnt = tps_cnt+1
        fail_cnt = fail_cnt + data[1]
        # print('data => {} / {}'.format(data, time.time() - before_time))


def print_stat(total_cnt, total_fail_cnt, tps_cnt, fail_cnt):
    now = time.localtime()
    print('%04d/%02d/%02d %02d:%02d:%02d ==> %6d / %-6d  |  %4d, %-4d' % (now.tm_year,
                                                                          now.tm_mon,
                                                                          now.tm_mday,
                                                                          now.tm_hour,
                                                                          now.tm_min,
                                                                          now.tm_sec,
                                                                          total_cnt,
                                                                          total_fail_cnt,
                                                                          tps_cnt,
                                                                          fail_cnt))


if __name__  == '__main__':
    procs = []
    start_time = int(time.time())
    q = Queue()

    # 작업 프로세스 실행
    for xx in range(0, 2):
        proc = Process(target=do_work, args=(xx,q))
        procs.append(proc)
        proc.start()

    # 통계 프로세스 실행
    proc_stat = Process(target=do_stat, args=(q,))
    proc_stat.start()

    # 작업 프로세스 종료 여부 확인
    for proc in procs:
        proc.join()

    # 통계 프로세스 종료 처리
    q.put([-1, -1])
    proc_stat.join()

    print('끝났네요..............')