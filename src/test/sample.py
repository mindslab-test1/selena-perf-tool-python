import time
from multiprocessing import Process, Queue
import math
import sys
from websocket import create_connection, WebSocket
import wave
import glob
import json
import ssl


# exec params
args = sys.argv[1:]
threadNum = int(args[0])
loopNum = int(args[1])
DIR_PATH = "D:/samsung_test/wav1/"
BUFFER_SIZE = 20480
## WS request params
# API_ID = "yge07230b4ee6f4723f"
# API_KEY = "73bf681094da4f1680b8150bef93026c"
# user_id = "ygeTest"
# answer_text = "blank"
# model = "wps"
answer_text = "Yes, I do"
channel = "contact_channel"
language = "eng"
user_id = "yTrzAsvsoTtTAkW"
biz = "BP0001"
v = "1"


def do_work(x, q):
    for i in range(0,loopNum):
        for path in glob.glob(DIR_PATH + "*.*"):
            # time.sleep(random.random())
            # print("현재 값은 ", i, "입니다.")
            webSocketConn(path, q)
            # q.put([1, 0])


def webSocketConn(filePath, q):
    # STT_URL = "wss://maieng.maum.ai:7777/engedu/v1/websocket/stt"
    # param = f"?apiId={API_ID}&apiKey={API_KEY}&userId={user_id}&model={model}&answerText={answer_text}"
    STT_URL = "wss://10.122.64.58:8080/uapi/stt/websocket/evaluationStt"
    # STT_URL = "wss://119.207.75.41:10080/stt/websocket/evaluationStt"
    param = f"?answerText={answer_text}&channel={channel}&language={language}&userId={user_id}&biz={biz}&v={v}&service="
    ws = websocket.WebSocket(sockopt={"cert_reqs": ssl.CERT_NONE})
    ws = ws.connect(STT_URL + param)
    fp = wave.open(filePath)
    frames_to_read = math.floor(BUFFER_SIZE / (fp.getsampwidth() + fp.getnchannels()))
    while True:
        frames = fp.readframes(frames_to_read)
        if not frames:
            break
        ws.send_binary(frames)
        time.sleep(0.01)
    fp.close()
    ws.send_binary(bytes(1))
    # print(ws.getstatus())
    # print(ws.getheaders())
    # print(ws.recv_frame())
    result = json.loads(ws.recv())
    if result['resCode'] == 200:
        # print("ok")
        q.put([1, 0])
    else:
        print("Response error : {0}".format(result['resCode']))
        q.put([1, 1])
    # result = ws.recv()
    # print("Received '%s'" % result["resCode"])
    ws.close()


def do_stat(q):
    total_cnt = 0
    total_fail_cnt = 0
    tps_cnt = 0
    fail_cnt = 0
    before_time = time.time()
    while True:
        if time.time() - before_time > 1:
            print_stat(total_cnt, total_fail_cnt, tps_cnt, fail_cnt)
            before_time = time.time()
            # print('data => {} / {}'.format(tps_cnt, fail_cnt))
            tps_cnt = 0
            fail_cnt = 0
        if q.qsize() == 0:
            continue
        data = q.get()
        if data[0] == -1 and data[1] == -1:
            print_stat(total_cnt, total_fail_cnt, tps_cnt, fail_cnt)
            break
        total_cnt = total_cnt + 1
        total_fail_cnt = total_fail_cnt + data[1]
        tps_cnt = tps_cnt + 1
        fail_cnt = fail_cnt + data[1]
        # print('data => {} / {}'.format(data, time.time() - before_time))


def print_stat(total_cnt, total_fail_cnt, tps_cnt, fail_cnt):
    now = time.localtime()
    print('%04d/%02d/%02d %02d:%02d:%02d ==> %6d / %-6d  |  %4d, %-4d' % (now.tm_year,
                                                                          now.tm_mon,
                                                                          now.tm_mday,
                                                                          now.tm_hour,
                                                                          now.tm_min,
                                                                          now.tm_sec,
                                                                          total_cnt,
                                                                          total_fail_cnt,
                                                                          tps_cnt,
                                                                          fail_cnt))


if __name__ == '__main__':
    procs = []
    start_time = int(time.time())
    q = Queue()
    # 작업 프로세스 실행
    for xx in range(0, threadNum):
        proc = Process(target=do_work, args=(xx, q))
        procs.append(proc)
        proc.start()
    # 통계 프로세스 실행
    proc_stat = Process(target=do_stat, args=(q,))
    proc_stat.start()
    # 작업 프로세스 종료 여부 확인
    for proc in procs:
        proc.join()
    # 통계 프로세스 종료 처리
    q.put([-1, -1])
    proc_stat.join()
    print('끝났네요..............')