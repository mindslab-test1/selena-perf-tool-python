import ssl
import websocket
import websockets
import math
import sys
import wave
import glob
import json
import time
import grpc

import w2l_pb2
import w2l_pb2_grpc

import stt_pb2
import stt_pb2_grpc

CHANNAL = "contact_channel"
LANGUAGE = "eng"
USER_ID = "BELSNAKE"
BIZ = "BP0001"
V = "1"
BUFFER_SIZE = 2048


def dummy_try_one(file_path):

    # 파일 전송
    fp = wave.open(file_path)
    frames_to_read = math.floor(BUFFER_SIZE / (fp.getsampwidth() + fp.getnchannels()))
    while True:
        frames = fp.readframes(frames_to_read)
        if not frames:
            break
        time.sleep(0.1)
    fp.close()

    # 결과 반환
    return 'succ'


def wss_try_one(host, file_path, answer_text, index, no):
    user_id = f"{USER_ID}_{index}_{no}"

    # URL 설정
    STT_URL = f"wss://{host}/uapi/stt/websocket/evaluationStt"
    PARAM = f"?answerText={answer_text}&channel={CHANNAL}&language={LANGUAGE}&userId={user_id}&biz={BIZ}&v={V}&service=&type=E&userText=&"

    # 웹소켓 연결
    wss = websocket.WebSocket(sslopt={"cert_reqs": ssl.CERT_NONE})
    wss.settimeout(10)
    wss.connect(STT_URL + PARAM)

    # 파일 전송
    fp = wave.open(file_path)
    frames_to_read = math.floor(BUFFER_SIZE / (fp.getsampwidth() + fp.getnchannels()))
    while True:
        frames = fp.readframes(frames_to_read)
        if not frames:
            break
        wss.send_binary(frames)
        time.sleep(0.01)
    fp.close()

    # 전송의 끝을 알림
    # wss.send_binary(bytes(1))

    # 결과 수신 및 단절
    try:
        raw_data = wss.recv()
    except websocket.WebSocketTimeoutException:
        print("Timeout Exception.. ", index, " : ", no)
        raw_data = "{\"resCode\":\"500\",\"resMsg\":\"SUCCESS\"}"

    if len(raw_data) == 0:
        raw_data = "{\"resCode\":\"500\",\"resMsg\":\"SUCCESS\"}"
    # print("data = ", raw_data)
    result = json.loads(raw_data)
    wss.close()

    # 결과 반환
    return result


# ######################################################################################################################
#
#

def generate_w2l_frames(file_path):
    # 파일 전송
    fp = wave.open(file_path)
    frames_to_read = math.floor(BUFFER_SIZE / (fp.getsampwidth() + fp.getnchannels()))
    while True:
        frames = fp.readframes(frames_to_read)
        if not frames:
            break
        speech = w2l_pb2.Speech(bin=frames)
        yield speech
    fp.close()


def grpc_cnn_try_one(host, file_path):
    channel = grpc.insecure_channel(host)
    stub = w2l_pb2_grpc.SpeechToTextStub(channel)
    result = ''

    speechs = generate_w2l_frames(file_path)

    grpc_results = stub.StreamRecognize(speechs)
    for grpc_result in grpc_results:
        # print(grpc_result.txt)
        result = grpc_result.txt

    # 전송의 끝을 알림
    # wss.send_binary(bytes(1))

    # 결과 수신 및 단절
    # raw_data = wss.recv()
    # if len(raw_data) == 0:
    #     raw_data = "{\"resCode\":\"500\",\"resMsg\":\"SUCCESS\"}"
    # print("data = ", raw_data)
    # result = json.loads(raw_data)
    channel.close()
    return result


# ######################################################################################################################
#
#

def generate_stt_frames(file_path):
    # 파일 전송
    fp = wave.open(file_path)
    frames_to_read = math.floor(BUFFER_SIZE / (fp.getsampwidth() + fp.getnchannels()))
    while True:
        frames = fp.readframes(frames_to_read)
        if not frames:
            break
        speech = stt_pb2.Speech(bin=frames)
        yield speech
    fp.close()


def grpc_dnn_try_one(host, file_path):
    channel = grpc.insecure_channel(host)
    stub = stt_pb2_grpc.SpeechToTextServiceStub(channel)
    result = ''

    speechs = generate_stt_frames(file_path)

    grpc_results= stub.StreamRecognize(speechs, metadata=(
            ('in.lang', 'eng'),
            ('in.samplerate', '16000'),
            ('in.model', 'wps'),
        ))

    for grpc_result in grpc_results:
        # print(grpc_result.txt)
        result = grpc_result.txt

    # 전송의 끝을 알림
    # wss.send_binary(bytes(1))

    # 결과 수신 및 단절
    # raw_data = wss.recv()
    # if len(raw_data) == 0:
    #     raw_data = "{\"resCode\":\"500\",\"resMsg\":\"SUCCESS\"}"
    # print("data = ", raw_data)
    # result = json.loads(raw_data)
    channel.close()
    return result