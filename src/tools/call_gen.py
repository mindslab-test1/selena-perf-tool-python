import try_one
import glob
import sys
import time
from multiprocessing import Process, Queue
import os
from urllib import parse


DIR_PATH = "D:/samsung_test/wav2/"

# WEB 테스트
# WSS_HOST = "10.122.64.58:8080"
WSS_HOST = "119.207.75.41:10080"

# GRPC_CNN 테스트
GRPC_CNN_HOST = "10.122.64.71:15001"
# GRPC_CNN_HOST = "119.207.75.54:15001"

# GRPC_DNN 테스트
# GRPC_DNN_HOST = "10.122.64.58:9801"
GRPC_DNN_HOST = "119.207.75.54:9801"


def do_callgen(q, idx, loop):
    count = 0

    for kk in range(loop):
        print( "loop.... ", "idx=", idx, " loop=", kk )

        for path in glob.glob(DIR_PATH + "*.wav"):
            # print("trying.... ", "idx=", idx, " count=", count)

            before_time = time.time()

            text_file = path.split(".")[0] + ".txt"
            f = open(text_file, 'r')
            answer_text = f.readline()
            f.close()
            # print( answer_text )

            count = count + 1
            # print(path)
            # try_one.try_one(WSS_HOST, path, path.split("\\")[1])

            # ==============================================================================================================
            #

            result = try_one.wss_try_one(WSS_HOST, path, parse.quote(answer_text), idx, count)
            if result["resCode"] != '200':
                q.put([1, 1])
            else:
                q.put([1, 0])
            # print( idx, count, time.time() - before_time, result["resCode"] )

            # ==============================================================================================================
            #

            # result = try_one.grpc_cnn_try_one(GRPC_CNN_HOST, path)
            # q.put([1, 0])
            # print( idx, count, time.time() - before_time, result )

            # ==============================================================================================================
            #

            # result = try_one.grpc_dnn_try_one(GRPC_DNN_HOST, path)
            # # print(idx, count, result)
            # q.put([1, 0])

            # ==============================================================================================================
            #

            # result = try_one.dummy_try_one(path)
            # q.put([1, 0])



def do_stat(q):
    total_cnt = 0
    total_fail_cnt = 0
    tps_cnt = 0
    fail_cnt = 0
    max_tps = 0

    print_stat_title()
    before_time = time.time()
    first_time = before_time
    while True:
        if time.time() - before_time > 1:
            # if int(time.time() - first_time) % 10 == 0:
            #     print_stat_title()

            if tps_cnt > max_tps:
                max_tps = tps_cnt

            print_stat(time.time() - first_time, total_cnt, total_fail_cnt, tps_cnt, max_tps)

            before_time = time.time()
            # print('data => {} / {}'.format(tps_cnt, fail_cnt))
            tps_cnt = 0
            fail_cnt = 0

        if q.qsize() == 0:
            continue

        data = q.get()
        if data[0] == -1 and data[1] == -1:
            print_stat(time.time() - first_time, total_cnt, total_fail_cnt, tps_cnt, max_tps)
            break

        total_cnt = total_cnt + 1
        total_fail_cnt = total_fail_cnt + data[1]
        tps_cnt = tps_cnt + 1
        fail_cnt = fail_cnt + data[1]
        # print('data => {} / {}'.format(data, time.time() - before_time))


def print_stat_title():
    print('%-19s %-12s  |  %-6s|  %-6s|  %-6s' % ('시간', '     전체 건수    ', '현재', '평균', '최대'))


def print_stat(total_sec, total_cnt, total_fail_cnt, tps_cnt, max_tps):
    now = time.localtime()
    print('%04d/%02d/%02d %02d:%02d:%02d ==> %6d / %-6d  |  %-6d  |  %-6d  |  %-6d' % (now.tm_year,
                                                                          now.tm_mon,
                                                                          now.tm_mday,
                                                                          now.tm_hour,
                                                                          now.tm_min,
                                                                          now.tm_sec,
                                                                          total_cnt,
                                                                          total_fail_cnt,
                                                                          tps_cnt,
                                                                          total_cnt/total_sec,
                                                                          max_tps ))


if __name__ == '__main__':
    procs = []
    start_time = int(time.time())
    q = Queue()

    # 작업 프로세스 실행
    for xx in range(0, 200):
        proc = Process(target=do_callgen, args=(q, xx, 100000))
        procs.append(proc)
        proc.start()

    # 통계 프로세스 실행
    proc_stat = Process(target=do_stat, args=(q,))
    proc_stat.start()

    # 작업 프로세스 종료 여부 확인
    for proc in procs:
        proc.join()

    # 통계 프로세스 종료 처리
    q.put([-1, -1])
    proc_stat.join()

