import ssl
import websocket
import math
import sys
import wave
import glob
import json
import time


answer_text = "Yes,%20I%20do"
channel = "contact_channel"
language = "eng"
user_id = "yTrzAsvsoTtTAkW"
biz = "BP0001"
v = "1"

DIR_PATH = "D:/samsung_test/wav1/"
BUFFER_SIZE = 20480


STT_URL = "wss://10.122.64.58:8080/uapi/stt/websocket/evaluationStt"
# STT_URL = "wss://119.207.75.41:10080/stt/websocket/evaluationStt"
param = f"?answerText={answer_text}&channel={channel}&language={language}&userId={user_id}&biz={biz}&v={v}&service=&type=E&userText=&"

print(STT_URL + param)

wss = websocket.WebSocket(sslopt={"cert_reqs": ssl.CERT_NONE})
wss.connect(STT_URL + param)

filePath = DIR_PATH + "BJH166_20190630233121_433.wav"

fp = wave.open(filePath)
frames_to_read = math.floor(BUFFER_SIZE / (fp.getsampwidth() + fp.getnchannels()))
while True:
    frames = fp.readframes(frames_to_read)
    if not frames:
        break
    wss.send_binary(frames)
    time.sleep(0.01)
fp.close()
wss.send_binary(bytes(1))

result = json.loads(wss.recv())
print(result)
if result['resCode'] == '200':
    print("ok")
    # q.put([1, 0])
else:
    print("Response error : {0}".format(result['resCode']))
    # q.put([1, 1])

wss.close()

